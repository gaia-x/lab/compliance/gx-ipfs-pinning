import { Controller, Get, Res } from '@nestjs/common'
import { Response } from 'express'
import { IpfsService } from './ipfs/ipfs.service.js'
@Controller()
export class AppController {
  constructor(private readonly ipfsService: IpfsService) {}

  async onApplicationShutdown(): Promise<void> {
    await this.ipfsService.onApplicationShutdown()
  }

  @Get()
  getDescription() {
    return {}
  }

  @Get('base-url')
  getBaseUrl(@Res() response: Response) {
    response.contentType('text/plain').send(process.env.BASE_URL)
  }
}
