import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'
import { AppController } from './app.controller.js'
import { DnsUpdateModule } from './dns-update/dns-update.module.js'
import { IpfsModule } from './ipfs/ipfs.module.js'

@Module({
  imports: [IpfsModule, DnsUpdateModule, ConfigModule.forRoot(), ScheduleModule.forRoot()],
  controllers: [AppController]
})
export class AppModule {}
