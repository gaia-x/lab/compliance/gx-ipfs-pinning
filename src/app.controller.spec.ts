import { ConfigService } from '@nestjs/config'
import { Test, type TestingModule } from '@nestjs/testing'
import { beforeEach, describe, expect, it } from 'vitest'
import { ConfigServiceMock } from '../test/utils/config-service.mock'
import { AppController } from './app.controller.js'
import { DnsUpdateService } from './dns-update/dns-update.service.js'
import { IpfsService } from './ipfs/ipfs.service.js'
import { StaticDirectoryProvider } from './ipfs/provider/static-directory.provider'

describe('AppController', () => {
  let appController: AppController

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [DnsUpdateService, IpfsService, ConfigService, new StaticDirectoryProvider()]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock()
          .setProperty('KUBO_HOST', 'localhost')
          .setProperty('APP_BRANCH', 'test')
          .setProperty('OCTODNS_REPO_PROTOCOL', 'https')
          .setProperty('OCTODNS_REPO_USERNAME', 'mock-username')
          .setProperty('OCTODNS_REPO_ACCESS_TOKEN', 'mock-pat')
          .setProperty('OCTODNS_REPO_URL', 'repotest.test/repo/path')
          .setProperty('OCTODNS_REPO_BRANCH', 'test-branch')
      )
      .compile()

    appController = app.get<AppController>(AppController)
  })

  it('should be defined', () => {
    expect(appController).toBeDefined()
  })
})
