import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import * as fs from 'fs'
import * as yaml from 'js-yaml'
import path from 'path'
import { rimraf } from 'rimraf'
import simpleGit, { SimpleGit } from 'simple-git'
import { fileURLToPath } from 'url'

const IPFS_URI_PREFIX = 'ipfs://'
const DOMAIN = 'gxdch.eu'

@Injectable()
export class DnsUpdateService {
  private readonly logger = new Logger(DnsUpdateService.name)

  private readonly appBranch: string
  private readonly branchName: string
  private readonly repoUrl: string

  private repoPath = this.getRepoPath()
  private configFilePath = `${this.repoPath}/config/gxdch.eu.yaml`
  private repoGit: SimpleGit

  constructor(private readonly configService: ConfigService) {
    this.appBranch = this.configService.getOrThrow('APP_BRANCH')
    this.branchName = this.configService.get('OCTODNS_REPO_BRANCH', 'main')

    const gitProtocol: string = this.configService.get('OCTODNS_REPO_PROTOCOL', 'https')
    const gitUsername: string = this.configService.getOrThrow('OCTODNS_REPO_USERNAME')
    const encodedPAT: string = this.encodePAT(this.configService.getOrThrow('OCTODNS_REPO_ACCESS_TOKEN'))
    const gitRepoUrl: string = this.configService.getOrThrow('OCTODNS_REPO_URL')
    this.repoUrl = `${gitProtocol}://${gitUsername}:${encodedPAT}@${gitRepoUrl}`
  }

  async cloneAndUpdateDnsRecord(updates: Array<{ domain: string; cidUri: string }>): Promise<void> {
    try {
      this.logger.log(`Cloning ${this.repoUrl}...`)
      await simpleGit().clone(this.repoUrl, this.repoPath)
      this.repoGit = simpleGit(this.repoPath)

      // Check out the branch, create it if it doesn't exist
      try {
        await this.repoGit.checkout(this.branchName)
      } catch {
        await this.repoGit.checkoutLocalBranch(this.branchName)
      }
      const fileContents = fs.readFileSync(this.configFilePath, 'utf8')
      const data = yaml.load(fileContents) as any

      let changesMade = false
      for (const update of updates) {
        // Update the in-memory data structure for each update
        const updateResult = this.updateTxtRecord(data, this.normalizeDomainName(update.domain), update.cidUri)
        if (updateResult) changesMade = true
      }

      if (changesMade) {
        const newYaml = yaml.dump(data)
        fs.writeFileSync(this.configFilePath, newYaml, 'utf8')
        this.logger.log('Updated DNS configuration file.')
        // Commit and push the changes
        await this.repoGit.add(this.configFilePath)
        await this.repoGit.commit(`Update IPFS TXT records for ${this.appBranch}`)
        await this.repoGit.push('origin', this.branchName)
        this.logger.log(`Commit pushed to ${this.branchName}`)
      } else {
        this.logger.log('No changes made to the DNS configuration.')
      }
    } catch (error) {
      this.logger.error('Failed to update DNS records:', error)
    } finally {
      // Delete the local clone after use
      this.logger.log('Deleting local repository')
      rimraf.sync(this.repoPath)
    }
  }

  private updateTxtRecord(data: any, domain: string, newValue: string): boolean {
    let updated = false

    // Ensure there's a record for the domain
    if (!data[domain]) {
      data[domain] = []
      updated = true // Mark as updated since we're adding a new domain record
    }

    let txtRecord = data[domain]?.find((record: { type: string }) => record.type === 'TXT')
    // If there's no TXT record, create one and add it
    if (!txtRecord) {
      txtRecord = { ttl: 600, type: 'TXT', values: [] }
      data[domain].push(txtRecord)
      updated = true // Mark as updated since we're adding a new TXT record
    }

    // Check if the record value needs to be updated
    const ipfsRecordIndex = txtRecord.values.findIndex((value: string) => value.startsWith(IPFS_URI_PREFIX))
    if (ipfsRecordIndex >= 0) {
      // Update if different
      if (txtRecord.values[ipfsRecordIndex] !== newValue) {
        txtRecord.values[ipfsRecordIndex] = newValue
        updated = true // Mark as updated since we're changing an existing value
      }
    } else {
      // Add new record value if it doesn't exist
      txtRecord.values.push(newValue)
      updated = true // Mark as updated since we're adding a new value
    }

    return updated
  }

  private getRepoPath(): string {
    // Convert the URL of the current module to a file path
    const currentDir = path.dirname(fileURLToPath(import.meta.url))
    return path.join(currentDir, '../octodns')
  }

  private encodePAT(pat: string): string {
    return encodeURIComponent(pat)
  }

  private normalizeDomainName(subdomain: string): string {
    // Check if the subdomain ends with the base domain and remove it
    if (subdomain.endsWith(`.${DOMAIN}`)) {
      return subdomain.substring(0, subdomain.lastIndexOf(`.${DOMAIN}`))
    }
    return subdomain
  }
}
