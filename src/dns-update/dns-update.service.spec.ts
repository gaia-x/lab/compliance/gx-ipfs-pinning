import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'
import * as fs from 'fs'
import * as yaml from 'js-yaml'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import { ConfigServiceMock } from '../../test/utils/config-service.mock'
import { DnsUpdateService } from './dns-update.service.js'

vi.mock('fs', async () => {
  const actual = (await vi.importActual('fs')) as typeof fs

  return {
    ...actual,
    readFileSync: vi.fn(),
    writeFileSync: vi.fn()
  }
})

vi.mock('js-yaml', () => ({
  load: vi.fn(),
  dump: vi.fn()
}))

vi.mock('simple-git', () => ({
  default: () => ({
    clone: vi.fn(),
    checkout: vi.fn(),
    checkoutLocalBranch: vi.fn(),
    add: vi.fn(),
    commit: vi.fn(),
    push: vi.fn()
  })
}))

vi.mock('rimraf', async () => {
  const actual = (await vi.importActual('rimraf')) as any
  return {
    ...actual,
    sync: vi.fn()
  }
})

vi.mock('path', async () => {
  const actual = (await vi.importActual('path')) as any
  return {
    ...actual,
    join: vi.fn(),
    dirname: vi.fn()
  }
})

describe('DnsUpdateService', () => {
  let service: DnsUpdateService

  beforeEach(async () => {
    vi.clearAllMocks()

    const module: TestingModule = await Test.createTestingModule({
      providers: [DnsUpdateService, ConfigService]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock()
          .setProperty('APP_BRANCH', 'branchName')
          .setProperty('OCTODNS_REPO_PROTOCOL', 'https')
          .setProperty('OCTODNS_REPO_USERNAME', 'mock-username')
          .setProperty('OCTODNS_REPO_ACCESS_TOKEN', 'mock-pat')
          .setProperty('OCTODNS_REPO_URL', 'repotest.test/repo/path')
          .setProperty('OCTODNS_REPO_BRANCH', 'test-branch')
      )
      .compile()

    service = module.get<DnsUpdateService>(DnsUpdateService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should clone the repository and update DNS records', async () => {
    const newIpfsValue = [
      {
        cidUri: 'ipfs://CIDNEW',
        domain: 'development.gxdch.eu'
      },
      {
        cidUri: 'ipfs://CIDNEW',
        domain: 'development._1.gxdch.eu'
      },
      {
        cidUri: 'ipfs://CIDNEW',
        domain: 'development._2.gxdch.eu'
      }
    ]

    ;(service as any).updateTxtRecord = vi.fn().mockResolvedValue('')
    ;(service as any).getRepoPath = vi.fn().mockResolvedValue('/repo/path')
    ;(service as any).getRepoUrlWithCredentials = vi.fn().mockResolvedValue('http://repotest.test')

    vi.mocked(fs.readFileSync).mockReturnValue('mocked yaml content')

    await service.cloneAndUpdateDnsRecord(newIpfsValue)
    const gitInstance = (service as any).repoGit

    expect(vi.mocked(fs.writeFileSync)).toHaveBeenCalled()
    expect(gitInstance.add).toHaveBeenCalledWith((service as any).configFilePath)
    expect(gitInstance.commit).toHaveBeenCalledWith('Update IPFS TXT records for branchName')
    expect(gitInstance.push).toHaveBeenCalledWith('origin', (service as any).branchName)
  })

  it('should correctly update the TXT record for a domain', () => {
    const data = {}
    const newValue = 'new-ipfs-value'
    ;(service as any).updateTxtRecord(data, 'testdomain.com', newValue)

    expect(data['testdomain.com']).toBeDefined()
    const txtRecord = data['testdomain.com'].find(record => record.type === 'TXT')
    expect(txtRecord).toBeDefined()
    expect(txtRecord.values).toContain(newValue)
  })

  it('should add a new TXT record if none exists', () => {
    const data = {}
    const newValue = 'new-ipfs-value'
    ;(service as any).updateTxtRecord(data, '', newValue)

    expect(data['']).toBeDefined()
    const txtRecord = data[''].find(record => record.type === 'TXT')
    expect(txtRecord).toBeDefined()
    expect(txtRecord.values).toContain(newValue)
  })

  it('should correctly modify the YAML configuration based on the updates', async () => {
    // Mock DNS configuration
    const mockDnsConfig = {
      '': [
        { ttl: 3600, type: 'A', values: ['213.186.33.5'] },
        { ttl: 600, type: 'TXT', values: ['4|gaia-x.eu'] }
      ],
      '*.azimov': [{ ttl: 3600, type: 'A', values: ['62.161.18.52'] }],
      www: [
        { ttl: 3600, type: 'A', values: ['213.186.33.5'] },
        { ttl: 600, type: 'TXT', values: ['4|gaia-x.eu'] }
      ]
    }

    vi.mocked(fs.readFileSync).mockReturnValue('mocked yaml content')
    vi.mocked(yaml.load).mockReturnValue(mockDnsConfig)

    const newIpfsValue = [
      { cidUri: 'ipfs://CIDNEW', domain: 'development.gxdch.eu' },
      { cidUri: 'ipfs://CIDNEW', domain: 'development._1.gxdch.eu' },
      { cidUri: 'ipfs://CIDNEW', domain: 'development._2.gxdch.eu' }
    ]

    await service.cloneAndUpdateDnsRecord(newIpfsValue)

    // Expected modified DNS configuration
    const expectedModifiedConfig = {
      // Original configuration...
      '': mockDnsConfig[''],
      '*.azimov': mockDnsConfig['*.azimov'],
      www: mockDnsConfig['www'],
      // Expected modifications based on newIpfsValue
      development: [{ ttl: 600, type: 'TXT', values: ['ipfs://CIDNEW'] }],
      'development._1': [{ ttl: 600, type: 'TXT', values: ['ipfs://CIDNEW'] }],
      'development._2': [{ ttl: 600, type: 'TXT', values: ['ipfs://CIDNEW'] }]
    }

    expect(vi.mocked(yaml.dump)).toHaveBeenCalledWith(expectedModifiedConfig)
  })
})
