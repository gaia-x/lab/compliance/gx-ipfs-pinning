import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { DnsUpdateService } from './dns-update.service.js'

@Module({
  imports: [ConfigModule],
  providers: [DnsUpdateService],
  exports: [DnsUpdateService]
})
export class DnsUpdateModule {}
