import { InjectionToken } from '@nestjs/common'
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface'
import path from 'path'
import { fileURLToPath } from 'url'

/**
 * Provides the absolute path to the <code>src/static/</code> directory as an injectable {@link string}.
 */
export class StaticDirectoryProvider implements FactoryProvider {
  provide: InjectionToken = 'StaticDirectory'

  useFactory(): string {
    const currentDir: string = path.dirname(fileURLToPath(import.meta.url))

    return path.join(currentDir, '..', '..', 'static')
  }
}
