import * as url from 'node:url'
import { vi } from 'vitest'
import { StaticDirectoryProvider } from './static-directory.provider'

vi.mock('url')

describe('StaticDirectoryProvider', () => {
  it('should return the absolute path to static/', () => {
    const provider: StaticDirectoryProvider = new StaticDirectoryProvider()

    vi.mocked(url.fileURLToPath).mockReturnValue('/absolute/path/to/current/directory')

    expect(provider.useFactory()).toEqual('/absolute/path/static')
  })
})
