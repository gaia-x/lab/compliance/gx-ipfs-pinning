import { Dirent, ObjectEncodingOptions, PathLike } from 'fs'
import * as fs from 'node:fs'
import path from 'path'
import { vi } from 'vitest'
import { IpfsModule } from './ipfs.module'

const staticDirectory = 'static/directory/'

vi.mock('fs')
type readDirWithType = (
  path: PathLike,
  options: ObjectEncodingOptions & {
    withFileTypes: true
    recursive?: boolean | undefined
  }
) => Dirent[]

type readDirWithoutType = (
  path: PathLike,
  options?:
    | {
        encoding: BufferEncoding | null
        withFileTypes?: false | undefined
        recursive?: boolean | undefined
      }
    | BufferEncoding
    | null
) => string[]

describe('IpfsModule', () => {
  const ipfsModule: IpfsModule = new IpfsModule(staticDirectory)

  const developmentDirectory: Dirent = {
    name: 'development',

    isDirectory(): boolean {
      return true
    }
  } as Dirent

  const lastestVersionDirectory: Dirent = {
    name: '2404',

    isDirectory(): boolean {
      return true
    }
  } as Dirent

  beforeEach(() => {
    vi.resetAllMocks()
  })

  it('should copy common static files to each static version directory', () => {
    vi.mocked(fs.readdirSync as unknown as readDirWithoutType).mockReturnValueOnce(['common.ttl', 'common.json', 'common.yaml', 'folder'])
    vi.mocked(fs.readdirSync as unknown as readDirWithType).mockReturnValueOnce([developmentDirectory, lastestVersionDirectory])

    ipfsModule.onModuleInit()

    const readDirCalls = vi.mocked(fs.readdirSync).mock.calls
    expect(readDirCalls).toHaveLength(2)
    expect(readDirCalls[0][0]).toMatch(/^.*\/common$/)
    expect(readDirCalls[0][1]).toBeUndefined()
    expect(readDirCalls[1][0]).toEqual(staticDirectory)
    expect(readDirCalls[1][1]).toEqual({ withFileTypes: true })

    expect(fs.cpSync).toHaveBeenCalledTimes(8)
    expect(vi.mocked(fs.cpSync).mock.calls).toEqual(
      expect.arrayContaining([
        [path.join(staticDirectory, 'common/common.ttl'), path.join(staticDirectory, 'development/common.ttl'), { recursive: true }],
        [path.join(staticDirectory, 'common/common.json'), path.join(staticDirectory, 'development/common.json'), { recursive: true }],
        [path.join(staticDirectory, 'common/common.yaml'), path.join(staticDirectory, 'development/common.yaml'), { recursive: true }],
        [path.join(staticDirectory, 'common/folder'), path.join(staticDirectory, 'development/folder'), { recursive: true }],
        [path.join(staticDirectory, 'common/common.ttl'), path.join(staticDirectory, '2404/common.ttl'), { recursive: true }],
        [path.join(staticDirectory, 'common/common.json'), path.join(staticDirectory, '2404/common.json'), { recursive: true }],
        [path.join(staticDirectory, 'common/common.yaml'), path.join(staticDirectory, '2404/common.yaml'), { recursive: true }],
        [path.join(staticDirectory, 'common/folder'), path.join(staticDirectory, '2404/folder'), { recursive: true }]
      ])
    )
  })

  it('should not copy anything when version directory is missing', () => {
    vi.mocked(fs.readdirSync as unknown as readDirWithoutType).mockReturnValueOnce(['common.ttl', 'common.json', 'common.yaml', 'folder'])
    vi.mocked(fs.readdirSync as unknown as readDirWithType).mockReturnValueOnce([])

    ipfsModule.onModuleInit()

    expect(fs.readdirSync).toHaveBeenCalledTimes(2)
    expect(fs.cpSync).not.toHaveBeenCalled()
  })

  it('should not copy anything when common directory is empty', () => {
    vi.mocked(fs.readdirSync as unknown as readDirWithoutType).mockReturnValueOnce([])

    ipfsModule.onModuleInit()

    expect(fs.readdirSync).toHaveBeenCalledTimes(1)
    expect(fs.cpSync).not.toHaveBeenCalled()
  })
})
