import { Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import * as dns from 'node:dns'
import fs from 'node:fs'
import path from 'path'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import { ConfigServiceMock } from '../../test/utils/config-service.mock'
import { DnsUpdateService } from '../dns-update/dns-update.service'
import { IpfsModule } from './ipfs.module'
import { IpfsService } from './ipfs.service.js'

const DOMAIN = 'gxdch.eu'

const ipfsClientMock = {
  addAll: vi.fn(),
  id: vi.fn().mockReturnValue({ id: 'test-peer-id' }),
  stop: vi.fn(),
  pin: {
    add: vi.fn()
  },
  dht: {
    provide: vi.fn()
  }
}

vi.mock('kubo-rpc-client', () => {
  return {
    create: vi.fn().mockImplementation(() => ipfsClientMock),
    globSource: vi.fn().mockResolvedValue({ path: 'some/path', cid: 'some-cid' }),
    CID: {
      parse: vi.fn().mockImplementation(cidString => {
        return {
          toString: vi.fn().mockReturnValue(cidString),
          equals: vi.fn().mockImplementation(otherCID => cidString === otherCID.toString())
        }
      })
    }
  }
})

vi.mock('../dns-update/dns-update.service.js', () => ({
  DnsUpdateService: vi.fn().mockImplementation(() => ({
    cloneAndUpdateDnsRecord: vi.fn()
  }))
}))

vi.mock('dns', () => ({
  resolveTxt: vi.fn((domain, callback) => {
    process.nextTick(() => {
      callback(null, [['DEFAULTTESTRECORD']])
    })
  })
}))

vi.mock('fs')

describe('IpfsService', () => {
  let service: IpfsService
  let dnsUpdateServiceMock: any

  beforeEach(async () => {
    vi.clearAllMocks()

    vi.mocked(fs.lstatSync).mockImplementation((statPath: string) => {
      const isFile = !!path.extname(statPath)

      return { isDirectory: () => !isFile } as fs.Stats
    })

    dnsUpdateServiceMock = {
      cloneAndUpdateDnsRecord: vi.fn()
    }

    const moduleRef = await Test.createTestingModule({
      imports: [IpfsModule]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock()
          .setProperty('KUBO_HOST', 'localhost')
          .setProperty('OCTODNS_GITLAB_PAT', 'test-pat')
          .setProperty('APP_BRANCH', 'development')
      )
      .overrideProvider(DnsUpdateService)
      .useValue(dnsUpdateServiceMock)
      .overrideProvider('StaticVersionDirectories')
      .useValue(['development', '2211'])
      .setLogger(new Logger())
      .compile()

    service = moduleRef.get<IpfsService>(IpfsService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should correctly start IPFS node', async () => {
    await service.onApplicationBootstrap()
    expect(service['ipfs'].id).toBeDefined()
  })

  it('should fetch registry resources and update DNS if needed', async () => {
    vi.mocked(ipfsClientMock.addAll).mockImplementationOnce(async function* () {
      yield { path: '', cid: 'CIDNEW' } // Simulate a new development/ folder CID
      yield { path: 'development', cid: 'development_CIDNEW' } // Simulate a new development/ folder CID
      yield { path: 'development/file.json', cid: 'file_CIDNEW' } // Simulate a new development/file.json CID
      yield { path: 'development/shapes', cid: 'development_shapes_CIDNEW' } // Simulate a new development/shapes/ folder CID
      yield { path: 'development/shapes/shape.ttl', cid: 'development_shapes_shape_CIDNEW' } // Simulate a new development/shapes/shape.ttl file CID
      yield { path: 'development/schemas', cid: 'development_schemas_CIDNEW' } // Simulate a new development/schemas/ folder CID
      yield { path: 'development/schemas/schema.jsonld', cid: 'development_schemas_schema_CIDNEW' } // Simulate a new development/schemas/schema.jsonld file CID
      yield { path: 'development/schemas/level-2', cid: 'development_schemas_level-2_CIDNEW' } // Simulate a new development/schemas/level-2 level-2 folder CID
      yield { path: '2211', cid: '2211_CIDNEW' } // Simulate a new 2211/ folder CID
      yield { path: '2211/shapes', cid: '2211_shapes_CIDNEW' } // Simulate a new 2211/shapes/ folder CID
      yield { path: '2211/schemas', cid: '2211_schemas_CIDNEW' } // Simulate a new 2211/schemas/ folder CID
    })

    await service.onApplicationBootstrap()

    expect(vi.mocked(dns.resolveTxt)).toHaveBeenCalledTimes(8)
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(1, 'development.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(2, 'development._development.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(3, 'development._development._shapes.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(4, 'development._development._schemas.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(5, 'development._development._schemas._level-2.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(6, 'development._2211.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(7, 'development._2211._shapes.' + DOMAIN, expect.anything())
    expect(vi.mocked(dns.resolveTxt)).toHaveBeenNthCalledWith(8, 'development._2211._schemas.' + DOMAIN, expect.anything())

    const expected = [
      {
        cidUri: 'ipfs://CIDNEW',
        domain: 'development.gxdch.eu'
      },
      {
        cidUri: 'ipfs://development_CIDNEW',
        domain: 'development._development.gxdch.eu'
      },
      {
        cidUri: 'ipfs://development_shapes_CIDNEW',
        domain: 'development._development._shapes.gxdch.eu'
      },
      {
        cidUri: 'ipfs://development_schemas_CIDNEW',
        domain: 'development._development._schemas.gxdch.eu'
      },
      {
        cidUri: 'ipfs://development_schemas_level-2_CIDNEW',
        domain: 'development._development._schemas._level-2.gxdch.eu'
      },
      {
        cidUri: 'ipfs://2211_CIDNEW',
        domain: 'development._2211.gxdch.eu'
      },
      {
        cidUri: 'ipfs://2211_shapes_CIDNEW',
        domain: 'development._2211._shapes.gxdch.eu'
      },
      {
        cidUri: 'ipfs://2211_schemas_CIDNEW',
        domain: 'development._2211._schemas.gxdch.eu'
      }
    ]
    expect(dnsUpdateServiceMock.cloneAndUpdateDnsRecord).toHaveBeenCalledWith(expected)
  })

  it('should not update DNS if the CID matches', async () => {
    vi.mocked(dns.resolveTxt).mockImplementation((_domain: string, callback) => {
      callback(null, [['CIDNEW']])
    })
    vi.mocked(ipfsClientMock.addAll).mockImplementationOnce(async function* () {
      yield { path: '', cid: 'CIDNEW' } // Simulate a new CID
    })

    await service.onApplicationBootstrap()

    expect(dnsUpdateServiceMock.cloneAndUpdateDnsRecord).not.toHaveBeenCalledWith('CIDNEW')
  })

  it('should handle errors when fetching TXT records', async () => {
    vi.mocked(ipfsClientMock.addAll).mockImplementationOnce(async function* () {
      yield { path: '', cid: 'root-cid' }
    })
    vi.mocked(dns.resolveTxt).mockImplementation((domain: string, callback) => {
      callback(new Error('DNS Error'), [[]])
    })

    expect(service.onApplicationBootstrap()).resolves.toBeUndefined()
  })

  it('should stop IPFS on application shutdown', async () => {
    await service.onApplicationBootstrap()
    await service.onApplicationShutdown()

    expect(service['ipfs'].stop).toHaveBeenCalled()
  })

  it('should add files to IPFS', async () => {
    vi.mocked(dns.resolveTxt).mockImplementation((domain: string, callback) => {
      callback(null, [['CIDNEW']])
    })
    vi.mocked(ipfsClientMock.addAll).mockImplementationOnce(async function* () {
      yield { path: 'some/path', cid: 'some-cid-file' }
      yield { path: '', cid: 'root-cid' }
    })

    await service.onApplicationBootstrap()

    expect((service as any).ipfs.addAll).toHaveBeenCalled()
  })
})
