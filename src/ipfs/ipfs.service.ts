import { Inject, Injectable, Logger, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Cron, CronExpression } from '@nestjs/schedule'
import { CID, IPFSHTTPClient, create, globSource } from 'kubo-rpc-client'
import { Version } from 'multiformats/dist/types/src/interface'
import * as dns from 'node:dns'
import fs from 'node:fs'
import path from 'path'
import { DnsUpdateService } from '../dns-update/dns-update.service.js'

const DOMAIN = 'gxdch.eu'
const IPFS_URI_PREFIX = 'ipfs://'

/**
 * Service in charge of managing IPFS file pinning.
 * <p>
 * Every version directory available in <code>src/static/</code> is collected and pinned.
 * </p>
 * <p>
 * Once every directory and file within those version directories is pinned, the corresponding DNS entries are queried
 * to check the <code>TXT</code> entry containing the previous IPFS CID. If the DNS entry and the new file/folder CID
 * don't match, the {@link DnsUpdateService} is called to update the entry in the OctoDNS repository.
 * </p>
 * <p>
 * Each DNS entry has the following format:
 * <pre>[version folder name](._[subfolder name])*.gxdch.eu</pre>
 * Please note that the subfolder name is not present in the root version folder DNS entry. And that a given DNS entry
 * can have multiple subfolder names in the case of multiple directory levels.
 * </p>
 */
@Injectable()
export class IpfsService implements OnApplicationBootstrap, OnApplicationShutdown {
  private readonly logger = new Logger(IpfsService.name)

  private readonly kuboHost: string
  private readonly appBranch: string

  private registryRootCID: CID<unknown, number, number, Version>
  private ipfs: IPFSHTTPClient

  constructor(
    private readonly configService: ConfigService,
    private readonly dnsUpdateService: DnsUpdateService,
    @Inject('StaticDirectory') private readonly staticDirectory: string
  ) {
    this.kuboHost = this.configService.getOrThrow('KUBO_HOST')
    this.appBranch = this.configService.get('APP_BRANCH') || 'development'
  }

  async onApplicationBootstrap(): Promise<void> {
    this.ipfs = create({
      host: this.kuboHost,
      port: 5001,
      protocol: 'http'
    })

    return await this.startNode()
  }

  public async onApplicationShutdown(): Promise<void> {
    if (this.ipfs != null) {
      await this.ipfs.stop()
    }
  }

  private async startNode(): Promise<void> {
    this.logger.log('Connecting to local IPFS node...')
    try {
      const id = await this.ipfs.id()
      this.logger.log(`Connected to IPFS node, peer id: ${id.id}`)
      return await this.fetchRegistryResources()
    } catch (error) {
      this.logger.error('Failed to connect to IPFS node:', error.stack)
    }
  }

  @Cron(CronExpression.EVERY_10_MINUTES)
  private async fetchRegistryResources(): Promise<void> {
    const updatesNeeded: Array<{ domain: string; cidUri: string }> = []

    for await (const file of this.ipfs.addAll(globSource(this.staticDirectory, '**[!common]/**/*'), { wrapWithDirectory: true })) {
      this.logger.log(`Found[${file.cid}]: ${file.path}`)

      await this.ipfs.pin.add(file.cid)
      this.ipfs.dht.provide(file.cid)

      const isDirectory: boolean = fs.lstatSync(path.join(this.staticDirectory, file.path)).isDirectory()
      if (file.path === '' || isDirectory) {
        const directoryCID: CID = file.cid // Capture the CID of the level-1 directory
        const subdomainPrefix: string = file.path
          .split('/')
          .map((directory: string) => (directory != '' ? `._${path.basename(directory)}` : directory))
          .join('')

        await this.calculateNeededDnsUpdates(subdomainPrefix, directoryCID, updatesNeeded)
      }
    }

    // Process updates
    try {
      await this.dnsUpdateService.cloneAndUpdateDnsRecord(updatesNeeded)
      this.logger.log('DNS record updated')
    } catch (error) {
      this.logger.error('Failed to update DNS record', error)
    }

    if (updatesNeeded.length === 0) {
      this.logger.log('No DNS records needed updating.')
    }
  }

  private async calculateNeededDnsUpdates(
    subdomainPrefix: string,
    rootDirCid: CID,
    updatesNeeded: Array<{
      domain: string
      cidUri: string
    }>
  ) {
    const recordDomain = `${this.appBranch}${subdomainPrefix}.${DOMAIN}`
    const records = await this.getTxtRecords(recordDomain)
    const registryRecord = records.flat().find(str => str.startsWith(IPFS_URI_PREFIX))

    const cidUri = IPFS_URI_PREFIX + rootDirCid
    if (registryRecord) {
      this.registryRootCID = CID.parse(registryRecord.substring(IPFS_URI_PREFIX.length))
      if (!this.registryRootCID.equals(rootDirCid)) {
        updatesNeeded.push({ domain: recordDomain, cidUri })
      }
    } else {
      updatesNeeded.push({ domain: recordDomain, cidUri })
    }
  }

  private async getTxtRecords(domain: string): Promise<string[][]> {
    this.logger.log(`Getting TXT records for : ${domain}`)

    return new Promise((resolve, reject) => {
      dns.resolveTxt(domain, (err, records) => {
        if (err) {
          this.logger.warn(`Couldn't get records for domain ${domain}, encountered error ${err.name} ${err.message}`)
          // Handle domain not found as a special case
          if (err.code === 'ENOTFOUND') {
            this.logger.warn(`Domain ${domain} does not exist yet in the DNS configuration. Proceeding with empty records.`)
            resolve([])
          } else {
            // other types of errors, reject the promise
            reject(err)
          }
        } else {
          // No error, resolve with the found records
          resolve(records)
        }
      })
    })
  }
}
