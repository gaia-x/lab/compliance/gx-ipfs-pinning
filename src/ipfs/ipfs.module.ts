import { Inject, Logger, Module, OnModuleInit } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import fs from 'node:fs'
import path from 'path'
import { DnsUpdateModule } from '../dns-update/dns-update.module.js'
import { IpfsService } from './ipfs.service.js'
import { StaticDirectoryProvider } from './provider/static-directory.provider.js'

@Module({
  imports: [DnsUpdateModule, ConfigModule],
  providers: [IpfsService, new StaticDirectoryProvider()],
  exports: [IpfsService]
})
export class IpfsModule implements OnModuleInit {
  private readonly logger = new Logger(IpfsModule.name)

  constructor(@Inject('StaticDirectory') private readonly staticDirectory: string) {}

  onModuleInit(): void {
    const commonDirectory: string = path.join(this.staticDirectory, 'common')
    const commonFiles: string[] = fs.readdirSync(commonDirectory)

    if (!commonFiles.length) {
      return
    }

    const versionDirectories: string[] = fs
      .readdirSync(this.staticDirectory, { withFileTypes: true })
      .filter(file => file.isDirectory() && file.name !== 'common')
      .map(dir => dir.name)

    for (const versionDirectory of versionDirectories) {
      for (const commonFile of commonFiles) {
        const source: string = path.join(commonDirectory, commonFile)
        const destination: string = path.join(this.staticDirectory, versionDirectory, commonFile)

        this.logger.log(`Copying ${source} to ${destination}...`)
        fs.cpSync(source, destination, { recursive: true })
      }
    }
  }
}
