# Gaia-x IPFS Pinning Service

The Gaia-X Lab IPFS Pinning Service is designed to be used by the Gaia-X AISBL to manage, update, and seed (through the management of an IPFS node) most of the artefacts needed by the Gaia-X Lab Registry Service.

It also leverages the gxdch.eu DNS TXT records to distribute the root content ID (an IPFS addressable hash) of the files to the deployed Gaia-x clearing houses registries.

The ontology, shapes, schemas are automatically updated from the Gaia-X Service Characteristics Working Group CI. The trusted issuers, and eventual revocation lists, are managed by the service. This service is not intended to be deployed by other parties than the Gaia-x AISBL.

Although the service in itself is centrally hosted, the artefacts are not (due to the very nature of the IPFS hashtable), and anyone can decide to use their own IPFS to help seed the files.

This service advertises IPFS content URI by gxdch version, ontology version and type (as first level folder structure found in src/static) as subdomains. Such as the shape folder in src/static/development/shapes, on the development branch for the 2404 ontology, would be advertised as: development.2404._shapes.gaia-x.eu TXT ipfs://[folder_content_id]

### Sequence diagram

```mermaid
sequenceDiagram
    Pinning Service->>+gxdch-version._ontology-version._type.gxdch.eu: Queries TXT fields
    gxdch-version._ontology-version._type.gxdch.eu-->>-Pinning Service: Root CID of stored compliance artefacts
    Pinning Service->>+Pinning Service: Compare gxdch-version._ontology-version._type.gxdch.eu records with local CID of compliance artefacts
    gxdch-version._ontology-version._type.gxdch.eu octodns repository->>+Pinning Service: Clone gxdch-version._ontology-version._type.gxdch.eu's octodns repository
    Pinning Service-->>-gxdch-version._ontology-version._type.gxdch.eu octodns repository: Update octodns configuration of TXT records if needed
    Pinning Service->>+IPFS network (through kubo deployment): Provide and pin compliance artefacts
    GXDCH Registry(ies) service(s)->>+gxdch-version._ontology-version._type.gxdch.eu: Regularly queries TXT records (cron job)
    gxdch-version._ontology-version._type.gxdch.eu-->>-GXDCH Registry(ies) service(s): Root CID of stored compliance artefacts
    GXDCH Registry(ies) service(s)->>+GXDCH Registry(ies) service(s): Compare gxdch-version._ontology-version._type.gxdch.eu TXT records with local CID of compliance artefacts
    GXDCH Registry(ies) service(s)->>+IPFS network (through kubo deployment): Queries artefacts if needed
    IPFS network (through kubo deployment)-->>-GXDCH Registry(ies) service(s): Provides compliance artefacts
    Other GXDCH services->>+GXDCH Registry(ies) service(s): Queries compliance artifacts through API
    GXDCH Registry(ies) service(s)-->>-Other GXDCH services: Provides compliance artifacts
```

### Existing deployments

This service has no ingress.

### Images tags

This repo provides
several [images tags](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/container_registry/5971030).

| tag           | content              | example |
| ------------- | -------------------- | ------- |
| `vX`          | latest major version | v1      |
| `vX.Y`        | latest minor version | v1.1    |
| `vX.Y.Z`      | specific version     | v1.1.1  |
| `main`        | latest stable        |         |
| `development` | latest unstable      |         |

Feature branches are also build and push to the container registry.

## Local Testing With Docker Compose

A [`docker-compose.yml` file](./docker-compose.yml) file is provided in this repository to allow developers to
conveniently run a minimal IPFS Pinning Service instance locally.

The following command will startup the necessary Docker containers.

```bash
docker-compose up -d
```

Once everything is up and running, you can check out the different services such as:
- the Kubo WebUI 👉 http://localhost:5001/webui
- the Gitea WebUI 👉 http://localhost:3001/
- the IPFS Pinning Service 👉 http://localhost:3000/

In a local testing environment the IPFS Pinning Service contacts the Gaia-X DNS server to check for missing
domains (this part doesn't interest us) to then pull and update the OctoDNS repository hosted on the Gitea
instance to reflect the state of the IPFS files exchanged with the Kubo instance.

By default, the Gitea instance is set up to provide the following credentials:

```
Username: ipfs-pinning-service
Password: testtest
Access Token: 89e961e329cc76dd91adf0550b458d0a4115c210
Project URL: http://localhost:3001/ipfs-pinning-service/octodns
```

When you are finished using your local testing environment, you can just stop it with the following command.

```bash
docker-compose down
```

## Deployment

A helm chart is provided inside `/k8s/gx-ipfs-pinning` folder. It deploys the IPFS pinning application and a kubo IPFS node server used
by the pinning service.

It provides several environment variables for the application:

| Env Variable              | Name in values file                         | Default value                 | Note                                                                                                             |
|---------------------------|---------------------------------------------|-------------------------------|------------------------------------------------------------------------------------------------------------------|
| APP_BRANCH                | {{ .Values.nameOverride }}                  | main                          | Deployment branch of the application                                                                             |
| KUBO_HOST                 | gx-registry-{{ .Values.nameOverride }}-kubo | gx-registry-main-kubo         | Valid Kubo instance deployed in the same namespace on the cluster                                                |
| OCTODNS_REPO_PROTOCOL     | octoDnsRepoProtocol                         | https                         | Protocol (HTTP, HTTPS, GIT-SSH) used to connect to to the OctoDNS repository managing the gxdch.eu domain        |
| OCTODNS_REPO_USERNAME     | octoDnsRepoUsername                         | oauth2                        | Username linked to the above access token used to connect to the OctoDNS repository managing the gxdch.eu domain |
| OCTODNS_REPO_ACCESS_TOKEN | octoDnsRepoAccessToken                      |                               | Valid Git Access Token for the OctoDNS repository managing the gxdch.eu domain                                   |
| OCTODNS_REPO_URL          | octoDnsRepoUrl                              | gitlab.com/gaia-x/lab/octodns | OctoDNS repository URL excluding the protocol part                                                               |
| OCTODNS_REPO_BRANCH       | octoDnsRepoBranch                           | main                          | Branch of the OctoDNS repository to which DNS changes will be pushed                                             |

Usage example:

```shell
helm upgrade --install -n "<branch-name>" --create-namespace gx-ipfs-pinning ./k8s/gx-ipfs-pinning --set "nameOverride=<branch-name>,octoDnsRepoAccessToken=$accessToken"
```

Deploy v2:

```shell
helm upgrade --install -n "v2" --create-namespace gx-ipfs-pinning ./k8s/gx-ipfs-pinning --set "nameOverride=v2,octoDnsRepoAccessToken=$accessToken"
```
