# [2.11.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.10.0...v2.11.0) (2025-02-19)


### Features

* **trusted-issuers:** add CISPE v2 ([2046294](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/2046294a8296ac7edcbcfb3859b58bf0e10e2328))

# [2.10.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.9.0...v2.10.0) (2025-02-19)


### Features

* **trusted-issuers:** add neusta v2, cispe v1, pfalzkom v1 ([80c3019](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/80c3019b15d6da4d73effe08128054c9c51acfc2))

# [2.9.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.8.0...v2.9.0) (2025-01-16)


### Features

* **trusted-issuers:** add DeltaDAO v2 ([848e826](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/848e826789e1a7321125876cff7412578bc47140))

# [2.8.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.7.0...v2.8.0) (2025-01-16)


### Features

* **trusted-issuers:** add DeltaDAO v2 ([c4ddb0d](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c4ddb0dd48e892bece53f2e9d45f97e62a698d44))

# [2.7.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.6.1...v2.7.0) (2025-01-03)


### Features

* **trusted-issuers:** update list ([8ab2207](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/8ab2207cae140c6705943295b63864b6161d5c2a))

## [2.6.1](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.6.0...v2.6.1) (2024-11-25)


### Bug Fixes

* bump vulnerable dependencies ([049da1e](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/049da1ee92e1750abb3e5a69d7b99758ab431271))

# [2.6.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.5.0...v2.6.0) (2024-10-31)


### Features

* release latest changes from development ([528c105](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/528c105e4b94d004ee84dc6a124de04556230925))

# [2.5.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.4.0...v2.5.0) (2024-10-18)


### Bug Fixes

* bump container image version to fix vulns ([c0df984](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c0df984a794766d14effa7aadd62f1976d05aaa7))


### Features

* add GXDCH ([0a47efe](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/0a47efe704f938c654d035f3e6bafbd11bd2dcb8))

# [2.4.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.3.0...v2.4.0) (2024-07-31)


### Bug Fixes

* **LAB-682:** set default Git protocol to https ([da1962e](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/da1962ebdb7f741e7d2fe102b6349cadb61523f7))


### Features

* **LAB-682:** change OctoDNS repo commit branch & introduce new env vars ([e32ca08](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/e32ca088edd861b41bca23e8e1a2c40bb0cb05fd))

# [2.3.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.2.0...v2.3.0) (2024-05-21)


### Features

* remove ipfs record prefix in the dns txt fields ([26476bd](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/26476bdf5151ca0f45c1349618eabd3a8e0ccf17))

# [2.2.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.1.0...v2.2.0) (2024-05-13)


### Bug Fixes

* **LAB-520:** fix JSON-LD context file extension ([6dfb568](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/6dfb5686bb15105f5dfe8e01b24e8638278b8649))
* **LAB-520:** fix JSON-LD context file extension (part 2) ([c0be7cf](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c0be7cf0205130b417ba393a8b8c77073243a207))
* **LAB-604:** add ConfigModule to DnsUpdateModule dependencies ([90cd15c](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/90cd15c3b0eb7665e1ede0c5377b78ceb58a8d8e))
* **LAB-604:** fix Dockerfile command ([0d2d174](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/0d2d174094c83081ac8aaaf0e8888b90611a476a))
* **LAB-604:** fix start:prod command with SWC compiler ([006551e](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/006551ed2a6d48f18d164f9593581d3b334386d0))
* **LAB-604:** move DNS update after OnModuleInit ([1d2e483](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/1d2e483fadb0e5e5de4025ab141ba7482eebd0dc))
* **LAB-604:** remove src/ folder from artifact paths ([2488ba1](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/2488ba1ff71faad1677c5fbc3ed5ffb94c80c2d5))
* **LAB-604:** restore wrongly deleted static files ([a0f1469](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/a0f14693d338239d9a000eb9fbbb0d9f37f2309c))


### Features

* **LAB-597:** pin the OWL ontology file ([08247ff](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/08247ff1f1f107a42be14265224825271bcbd2be))
* **LAB-598:** remove the from ontology version name ([6661d3c](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/6661d3c6ffba997478569d522eb3e7e831484c0d))
* **LAB-604:** copy common static files to static version directories ([a286b8a](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/a286b8afe23f2f55589ce070bca408d4699f57a6))
* **LAB-604:** introduce artifact paths environment variable ([7d5aa1d](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/7d5aa1d4e2777102d20c54d322fa548b793abf0f))
* **LAB-604:** introduce ConfigService best practice ([ba2a461](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/ba2a4619db54440463f98486c2303269a5004b39))
* **LAB-608:** change DNS entry and static file management ([9ac9480](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/9ac9480209c9a19a78d2e2aa507a56b039a08947))

# [2.1.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v2.0.0...v2.1.0) (2024-04-16)


### Bug Fixes

* allow creation of new subdomains configuration ([b15dc13](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/b15dc135054da2d3ccd992f07751e132e01682ec))
* fix remove trailing underscore for ovh domains configuration ([eb7b07e](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/eb7b07e895e1c1239fc430cf1052adb5932e900e))
* **LAB-545:** fix CriteriaEvidenceShape description ([c6a7742](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c6a774224dbed6a44a3129c3cc81ffee709bf9e5))
* **LAB-590:** create missing folders for artifacts ([c9ddfda](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c9ddfdab99faf40feb98e588214f2dad789c919c))
* **LAB-590:** disable ontology collection for basic pipelines ([20b663d](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/20b663d213703f34ecdf8a789f03ea47de4e5e03))
* **LAB-590:** fix Git remote URL for pipeline ([36be8bc](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/36be8bcb1afb6804e45805edc6e7dc3012afe172))
* **LAB-596:** add a wildcard for NestJS asset copying ([8914fbe](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/8914fbe7868040af8a8f45a76680c9a9101fa7be))
* normalize subdomain to exclude main domains ([9ce804f](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/9ce804fa09326540bd9a8a0409e12690c8f94a07))


### Features

* **LAB-503:** pinning as URI by document and environment ([a109116](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/a1091162398c5c2bbd1e2b056395adfdda43caaa))
* **LAB-590:** collect ontology through pipeline ([bcc1c74](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/bcc1c7495040d9aeeaa7c807d29c41b1bff978bb))
* remove standalone kubo deployment to use gx-registry ([9c0078a](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/9c0078a01906ac216d72e9d088600e9f78f4b66f))

# [2.0.0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/compare/v1.0.0...v2.0.0) (2024-02-06)


### chore

* release ([c777bb0](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning/commit/c777bb0982c4a4f70c1bd9307fdce520c48d0d1a))


### BREAKING CHANGES

* bump to v2
