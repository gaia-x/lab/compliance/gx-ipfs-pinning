stages:
  - lint
  - test
  - sonar
  - build
  - scan
  - ontology
  - release
  - deploy

variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: '/certs'
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  # Below is only used in deploy-tag-on-lab
  ONTOLOGY_VERSION:
    value: '2406'
    description: 'The version of the Gaia-X ontology, for example 24.04 or 22.10'

.setup-ssh:
  image: node:20-alpine
  before_script:
    - apk update && apk add openssh-client git
    - git config --global user.email "cto@gaia-x.eu"
    - git config --global user.name "semantic-release-bot"
    - git checkout "$CI_COMMIT_REF_NAME"
    - git remote set-url origin git@gitlab.com:gaia-x/lab/compliance/gx-ipfs-pinning.git

    - eval $(ssh-agent -s)
    - echo "$CI_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh

    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

lint:code:
  image: node:20-buster
  stage: lint
  script:
    - npm ci
    - npm run lint
  rules:
    - if: $CI_PIPELINE_SOURCE != "pipeline"

test:
  image: node:20-buster
  stage: test
  script:
    - npm ci
    - npm run test
  rules:
    - if: $CI_PIPELINE_SOURCE != "pipeline"
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    when: on_success
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/unit/cobertura-coverage.xml
    paths:
      - coverage/unit/lcov.info

sonarqube-check:
  stage: sonar
  image:
    name: registry.gitlab.com/gaia-x/lab/devops/sonar-custom-image:latest
    entrypoint: [ "" ]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - npm ci --cache .npm --prefer-offline
    - sonar-scanner
  allow_failure: true
  only:
    - development

##Build and deploy branches
build:
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE --target production-build-stage .
    - docker push $CONTAINER_TEST_IMAGE
  rules:
    - if: $CI_PIPELINE_SOURCE != "pipeline"

# Collect the ontology files from the Service Characteristics Working Group repository
collect-ontology:
  stage: ontology
  extends: .setup-ssh
  script:
    # IPFS_ONTOLOGY_VERSION comes from the upstream pipeline.
    # Please see https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blob/develop/.gitlab-ci.yml
    - ONTOLOGY_VERSION=${IPFS_ONTOLOGY_VERSION//.}
    - mkdir -p src/static/$ONTOLOGY_VERSION/shapes/
    - mv shapes.shacl.ttl src/static/$ONTOLOGY_VERSION/shapes/trustframework.ttl
    - mkdir -p src/static/$ONTOLOGY_VERSION/schemas/
    - mv context.jsonld src/static/$ONTOLOGY_VERSION/schemas/trustframework.json
    - mkdir -p src/static/$ONTOLOGY_VERSION/owl/
    - mv ontology.owl.ttl src/static/$ONTOLOGY_VERSION/owl/trustframework.ttl
    - mkdir -p src/static/$ONTOLOGY_VERSION/linkml/
    - mv linkml.yaml src/static/$ONTOLOGY_VERSION/linkml/linkml.yaml
    - git add src/static/$ONTOLOGY_VERSION/shapes/trustframework.ttl src/static/$ONTOLOGY_VERSION/schemas/trustframework.json src/static/$ONTOLOGY_VERSION/owl/trustframework.ttl src/static/$ONTOLOGY_VERSION/linkml/linkml.yaml
    - 'git commit -m "chore: update ${ONTOLOGY_VERSION} ontology schemas and shapes"'
    - git push
  needs:
    - project: gaia-x/technical-committee/service-characteristics-working-group/service-characteristics
      job: build-linkml
      ref: $UPSTREAM_REF
      artifacts: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline"

deploy-on-lab:
  image: ubuntu
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_COMMIT_BRANCH == "development" && $CI_PIPELINE_SOURCE != "pipeline"
  before_script:
    - apt update && apt install -y curl
    - curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  script:
    - helm upgrade --install -n "$CI_COMMIT_REF_SLUG" --create-namespace gx-ipfs-pinning ./k8s/gx-ipfs-pinning --set "nameOverride=$CI_COMMIT_REF_SLUG,image.tag=$CI_COMMIT_REF_SLUG,octoDnsRepoAccessToken=$accessToken,octoDnsRepoBranch=main" --kubeconfig "$GXDCH_KUBECONFIG"

# Semantic release on main
make-release:
  stage: release
  extends: .setup-ssh
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main"
  script:
    - npm ci
    - ./node_modules/.bin/semantic-release

## Tags build & deploy
build-and-tag-release-image:
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --target production-build-stage .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG} $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG%.*} #vX.Y tag
    - docker push $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG%.*}
    - docker tag $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG} $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG%%.*} #vX tag
    - docker push $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG%%.*}
  only:
    - tags

deploy-tag-on-lab:
  image: ubuntu
  stage: deploy
  when: manual
  before_script:
    - apt update && apt install -y curl
    - curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  script:
    - if [[ -z $ONTOLOGY_VERSION ]]; then echo "Please provide a valid ONTOLOGY_VERSION variable"; exit 1 ; fi
    - helm upgrade --install -n "${CI_COMMIT_TAG%%.*}" --create-namespace gx-ipfs-pinning ./k8s/gx-ipfs-pinning --set "nameOverride=${CI_COMMIT_TAG%%.*},image.tag=${CI_COMMIT_TAG%%.*},octoDnsRepoAccessToken=$accessToken,octoDnsRepoBranch=main" --kubeconfig "$GXDCH_KUBECONFIG"
  rules:
    - if: $CI_COMMIT_TAG
      when: manual

include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

sast:
  stage: test

container_scanning:
  stage: scan
  variables:
    GIT_STRATEGY: fetch
    CS_IMAGE: $CONTAINER_TEST_IMAGE
