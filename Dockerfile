FROM node:22-alpine AS development-build-stage

RUN apk upgrade --no-cache && apk add git && apk cache clean

USER node
WORKDIR /usr/src/app

RUN git config --global user.email "cto@gaia-x.eu" && git config --global user.name "gx-ipfs-pinning"

COPY --chown=node package*.json ./
COPY --chown=node tsconfig.json tsconfig.json
COPY --chown=node tsconfig.build.json tsconfig.build.json
COPY --chown=node nest-cli.json nest-cli.json
COPY --chown=node vitest.config.ts vitest.config.ts

RUN npm install

COPY --chown=node src src

RUN npm run build

FROM node:22-alpine AS production-build-stage

ENV NODE_ENV=production

RUN apk upgrade --no-cache &&  apk add git && apk cache clean

USER node
WORKDIR /usr/src/app

RUN git config --global user.email "cto@gaia-x.eu" && git config --global user.name "gx-ipfs-pinning"

COPY --chown=node package*.json ./
COPY --chown=node --from=development-build-stage /usr/src/app/dist dist

RUN npm install --only=production

CMD ["node", "dist/src/main"]
