export class ConfigServiceMock {
  private readonly properties: Map<string, string> = new Map<string, string>()

  setProperty(property: string, value: string): ConfigServiceMock {
    this.properties.set(property, value)

    return this
  }

  get(property: string): string | undefined {
    return this.properties.get(property)
  }

  getOrThrow(property: string): string | undefined {
    if (this.properties.get(property)) {
      return this.properties.get(property)
    }

    throw new Error(`Missing property ${property} !`)
  }
}
